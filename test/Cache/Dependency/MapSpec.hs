{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TypeApplications  #-}

module Cache.Dependency.MapSpec where

import           Cache
import           Cache.Dependency.Types
import qualified Data.Map               as M
import           Polysemy
import           Polysemy.State
import           Test.Hspec

spec :: Spec
spec = do
  describe "runMapDependencyCache" $ do
    context "DeleteKeys" $ do
      it "removes a key" $
        let init :: DependencyMap
            init = [("deleteMe", undefined)]
            res = run' init $ deleteKeys ["deleteMe"]
        in fst res `shouldBe` []

      it "leaves other keys alone" $
        let init :: DependencyMap
            init = [("deleteMe", undefined)
                   ,("keepMe", undefined)
                   ]
            res = run' init $ deleteKeys ["deleteMe"]
        in M.keys (fst res) `shouldBe` ["keepMe"]

    context "SetVal" $ do
      it "creates a key" $ do
        let (res,_) = run' [] $ setVal "xyz" [Upstream "uuu"] "val"
        M.keys res `shouldBe` ["xyz"]
        let [(_,Cached{..})] = M.toList res
        cachedVal `shouldBe` "val"
        cachedUpstream `shouldBe` [Upstream "uuu"]

    context "LookupCached" $ do
      it "returns a found value" $
        let cached = Cached
              { cachedVal = "val"
              , cachedUpstream = [Upstream "uuu"]
              , cachedDownstream = [Downstream "ddd"]
              }

            init :: DependencyMap
            init = [("key", cached)]

            (_,cached') = run' init $ lookupCached "key"

        in cached' `shouldBe` Just cached

    context "SetDependency" $ do
      it "adjusts an upstream dependency" $
        let cached = Cached
              { cachedVal = "val"
              , cachedUpstream = []
              , cachedDownstream = []
              }

            init :: DependencyMap
            init = [("uuu", cached)]

            (res, _) = run' init $ setDependency (Downstream "ddd") (Upstream "uuu")
        in res `shouldBe`
             [("uuu",cached
                { cachedDownstream = [Downstream "ddd"]
                }
              )]

run' init = run . runState @DependencyMap init . runMapDependencyCache
