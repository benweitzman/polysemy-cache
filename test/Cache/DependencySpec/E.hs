{-# LANGUAGE TemplateHaskell #-}

module Cache.DependencySpec.E where

import           Data.Aeson
import           Polysemy
import           Polysemy.Distributed
import Polysemy.State
import Cache
import Cache.Types

data E (m :: * -> *) a where
  DoAThing :: Int -> E m Bool

makeSem ''E

runE
  :: Member Cache r
  => Sem (E ': r) a -> Sem r a
runE = interpret $ \case
  DoAThing n -> cache ("DoAThing-" ++ show n)$
    return $ even n

deriving instance Show (E m a)

instance ToJSON (E m a) where
  toJSON (DoAThing s) = object
    ["tag" .= ("DoAThing" :: String)
    ,"contents" .= s
    ]

instance TransJSON E where
  transFrom DoAThing{} = id
  transTo DoAThing{} = id

instance ParseEffect E where
  effectParsers =
    [ ExistentialParser $ withObject "DoAThing" $ \o -> do
        ("DoAThing" :: String) <- o .: "tag"
        DoAThing <$> o .: "contents"
    ]

instance ToJSON s => ToJSON (State s m a) where
  toJSON Get = object
    ["tag" .= ("Get" :: String)
    ]
  toJSON (Put s) = object
    ["tag" .= ("Put" :: String)
    ,"contents" .= s
    ]

instance (ToJSON s, FromJSON s) => TransJSON (State s) where
  transFrom Get = id
  transFrom Put{} = id

  transTo Get = id
  transTo Put{} = id

instance FromJSON s => ParseEffect (State s) where
  effectParsers =
    [ ExistentialParser $ withObject "Get" $ \o -> do
        ("Get" :: String) <- o .: "tag"
        return Get
    , ExistentialParser $ withObject "Put" $ \o -> do
        ("Put" :: String) <- o .: "tag"
        Put <$> o .: "contents"
    ]
