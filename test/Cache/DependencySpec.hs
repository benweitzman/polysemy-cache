{-# LANGUAGE DataKinds        #-}
{-# LANGUAGE OverloadedLists  #-}
{-# LANGUAGE RecordWildCards  #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -fno-warn-partial-type-signatures #-}

module Cache.DependencySpec where

import           Cache
import           Cache.Dependency
import           Cache.DependencySpec.E
import           Cache.Types
import           Data.IORef
import qualified Data.Map                    as M
import qualified Data.Set                    as S
import           Polysemy
import           Polysemy.Distributed
import           Polysemy.Distributed.Vessel
import           Polysemy.HTTP
import           Polysemy.Observe
import           Polysemy.Output
import           Polysemy.State
import           Polysemy.Writer
import           Test.Hspec

spec :: Spec
spec = do
  describe "runDependencyCache" $ do
    it "gives back a generated value" $ do
      let res :: Int
          res = run' [] $ cache "key" $ return 10
      res `shouldBe` 10

    it "doesn't execute subsequent uses" $ do
      let res :: Int
          res = run' [] $ do
            cache "key" $ return (10 :: Int)
            cache "key" $ undefined

      res `shouldBe` 10

    it "allows fields to be changed" $ do
      let res :: Int
          res = run' [] $ do
            cache "key" $ return (9 :: Int)
            unset "key"
            cache "key" $ return 10

      res `shouldBe` 10

    it "can handle nested caches" $ do
      let res :: Int
          res = run' [] $ cache "a" $ do
            b <- cache "b" $ return 1
            return $ b + 17
      res `shouldBe` 18

    it "caches nested values" $ do
      let res :: (Int, Int)
          res = run' [] $ do
            cache "a" $ do
              b <- cache "b" $ return (1 :: Int)
              return $ b + 17
            a <- cache "a" undefined
            b <- cache "b" undefined
            return (a, b)
      res `shouldBe` (18, 1)

    it "unsets nested cached values" $ do
      let res :: (Int, Int)
          res = run' [] $ do
            cache "a" $ do
              b <- cache "b" $ return (1 :: Int)
              return $ b + 17
            unset "b"
            a <- cache "a" $ return 91
            b <- cache "b" $ return 84
            return (a, b)
      res `shouldBe` (91, 84)

    it "leaves upstream cached values alone" $ do
      let res :: Int
          res = run' [] $ do
            cache "a" $ do
              b <- cache "b" $ return (1 :: Int)
              return $ b + 17
            unset "a"
            cache "b" undefined
      res `shouldBe` 1

    it "handles a diamond dependency" $ do
      let res :: Int
          res = run' [] $ do
            cache "d" $ do
              b <- cache "b" $ do
                a <- cache "a" $ return 25
                return $ a + 9
              c <- cache "c" $ do
                a <- cache "a" $ undefined
                return $ a + 8
              return $ c * b
      res `shouldBe` 1122

    it "translates dependencies of cached keys" $ do
      let res :: Int
          res = run' [] $ do
            cache "b" $ do
              a <- cache "a" $ do
                return (1 :: Int)
              return $ a + 9
            cache "c" $ do
              b <- cache "b" undefined
              return $ b + (27 :: Int)
            unset "a"
            cache "c" $ return (-1)
      res `shouldBe` -1

{-
-- It would be nice if this next test worked, but it doesn't.
-- We can say for now that a state where a key that is being "unset" is missing is
-- illegal. Using `insertCache` from the root Cache module should ensure that all keys
-- which can be unset are treated as critical data
    it "can tolerate missing data" $ do
      let init :: DependencyMap
          init = [("a", Cached
                    { cachedVal = undefined
                    , cachedUpstream = []
                    , cachedDownstream = [Downstream "b", Downstream "c"]
                    })
                  ,("c", Cached
                     { cachedVal = undefined
                     , cachedUpstream = [Upstream "b", Upstream "a"]
                     , cachedDownstream = []
                     })
                  ]

          res :: Int
          res = run' init $ do
            unset "b"
            cache "c" $ return 3
      res `shouldBe` 3
-}

  describe "distributed" $ do
    it "should work" $ do
      hostStateRef <- newIORef @DependencyMap []
      let hostApp = distributedVesselEffectApplication @'[E] $
            runStateIORef hostStateRef
            . runMapDependencyCache
            . shareVessel
            . runVesselDependencyCache
            . runE

          run :: Sem _ a -> IO a
          run = runM @IO
            . runStateIORef hostStateRef
            . raiseClientExceptions
            . runMapDependencyCache
            . shareVessel
            . ignoreOutput @(Deposit Server)
            . runVesselDependencyCache
            . applicationHTTP hostApp
            . runDistributedVesselEffect @E "http://localhost:80"

      res1 <- run $ do
        cache "client" $ do
          doAThing 34

      run $ unset "DoAThing-34"

      res2 <- run $ do
        cache "client" $ do
          doAThing 35

      (res1, res2) `shouldBe` (True, False)


run' :: DependencyMap -> Sem '[Cache, DependencyCache, State DependencyMap] a -> a
run' init = snd
  . run
  . runState @DependencyMap init
  . runMapDependencyCache
  . runDependencyCache
