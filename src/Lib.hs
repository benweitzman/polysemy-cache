{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE Rank2Types          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TupleSections       #-}
{-# LANGUAGE TypeOperators       #-}

module Lib where

import Cache
import Polysemy
import Polysemy.Internal.CustomErrors
import Polysemy.Internal
import Control.Monad
import Debug.Trace
import Unsafe.Coerce
import Data.Binary (Binary)

someFunc :: IO ()
someFunc = putStrLn "someFunc"

data User m a where
  SetUserProp :: Int -> Int -> User m Int
  GetUserProp :: Int -> User m Int

makeSem ''User

runUser :: Member Cache r => Sem (User ': r) a -> Sem r a
runUser = interpret $ \case
  GetUserProp uId -> undefined
  SetUserProp uId prop -> return prop


userCache :: User m a -> CacheBehavior a
userCache (GetUserProp uId) = ImmutableCache $ "XXuser:" ++ show uId
userCache (SetUserProp uId _) = MutableCache $ "XXuser:" ++ show uId

data DerivedUser m a where
  LookupUser :: Int -> DerivedUser m Int

makeSem ''DerivedUser

derivedUserCache :: DerivedUser m a -> CacheBehavior a
derivedUserCache (LookupUser uId) = ImmutableCache $ "XXderived:" ++ show uId

runDerivedUser :: Members '[User] r => Sem (DerivedUser ': r) a -> Sem r a
runDerivedUser = interpret $ \case
  LookupUser id -> do
    prop <- getUserProp id
    return (prop + 5)


test :: Members '[User, DerivedUser, Embed IO, Cache] r => Sem r Int
test = do
  setUserProp 1 100
  getUserProp 1
  lookupUser 1
  setUserProp 1 200
  getUserProp 1
  lookupUser 1
