{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Cache (
  CacheBehavior(..),
  insertCache,
  Cache,
  module Cache.Dependency
  ) where

import Cache.Dependency
import Cache.Types
import Data.Binary (Binary)
import Polysemy
import Polysemy.Internal
import Polysemy.Internal.CustomErrors
import Unsafe.Coerce

data CacheBehavior x where
  ImmutableCache :: (Binary x, Show x) => String -> CacheBehavior x
  MutableCache :: (Binary x, Show x) => String -> CacheBehavior x
  NoCache :: CacheBehavior x

insertCache :: forall e r a
               . (Members '[e, Cache] r, FirstOrder e "intercept")
            => (forall m x . e m x -> CacheBehavior x) -> Sem r a -> Sem r a
insertCache f = intercept @e $ \e -> do
  let e' :: e (Sem r) x
      e' = unsafeCoerce e
  case f e of
    NoCache -> send e'
    ImmutableCache key -> cache key $ send e'
    MutableCache key -> do
      unset key
      cache key $ send e'
