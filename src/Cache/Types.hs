{-# LANGUAGE TemplateHaskell #-}

module Cache.Types where

import           Data.Binary                    (Binary)
import           Polysemy
import           Polysemy.Distributed
import           Polysemy.Distributed.Vessel

data Cache m a where
  Cache :: (Binary a, Show a) => String -> m a -> Cache m a
  Unset :: String -> Cache m ()

makeSem ''Cache

instance VesselAccess Cache where
  data instance Key Cache s a where
    CacheDependencies :: Key Cache "X-Cache-Dependencies" [String]
