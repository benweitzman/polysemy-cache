module Cache.Dependency
  (runDependencyCache
  ,DependencyCache
  ,Cached(..)
  ,DependencyMap
  ,Upstream(..)
  ,Downstream(..)
  ,runMapDependencyCache
  ,runRedisDependencyCache
  ,runVesselDependencyCache
  ) where

import           Cache.Dependency.Map
import           Cache.Dependency.Redis
import           Cache.Dependency.Types
import           Cache.Types
import           Control.Monad
import qualified Data.Binary as B
import           Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import           Polysemy
import           Polysemy.State
import Polysemy.Output
import Polysemy.Observe
import Polysemy.Internal
import Polysemy.Writer
import Polysemy.Distributed.Vessel

runDependencyCache
  :: Member DependencyCache r
  => Sem (Cache ': r) a
  -> Sem r a
runDependencyCache = fmap snd . runDependencyCache'

runDependencyCache'
  :: Member DependencyCache r
  => Sem (Cache ': r) a
  -> Sem r ([Upstream], a)
runDependencyCache' =
  runWriter
  . observeOutputToWriter
  . runDependencyCache''
  . raiseUnder3

runDependencyCache''
  :: (Members '[DependencyCache, Output [Upstream], Observe [Upstream]] r)
  => Sem (Cache ': r) a
  -> Sem r a
runDependencyCache'' = interpretH $ \case
  Unset key -> do
    mFound <- lookupCached key
    forM_ mFound $ \Cached{..} -> do
      deleteKeys $ key : (getDownstreamKey <$> cachedDownstream)
    pureT ()

  Cache key gen -> do
    mExisting <- lookupCached key
    case mExisting of
      Just Cached{..} -> do
        output cachedUpstream
        output [Upstream key]
        let decoded = B.decode $ LBS.fromStrict cachedVal
        pureT decoded
      Nothing -> do
        g <- runT gen
        (upstream, val) <- raise $ runDependencyCache'' $ observe g
        ins <- getInspectorT
        case inspect ins val of
          Just x -> do
            let encoded = LBS.toStrict $ B.encode x
            setVal key upstream encoded
            forM upstream $ setDependency (Downstream key)
        output [Upstream key]
        return val

runVesselDependencyCache
  :: Members '[Output (Vessel Server), Output (Deposit Server), DependencyCache] r
  => Sem (Cache ': r) a
  -> Sem r a
runVesselDependencyCache =
  fmap snd
  . returnVesselCache
  . runWriter
  . observeOutputToWriter
  . observeVesselCache
  . runDependencyCache''
  . raiseUnder3 @(Observe [Upstream]) @(Output [Upstream]) @(Writer [Upstream])

returnVesselCache
  :: Members '[Output (Deposit Server)] r
  => Sem r ([Upstream], a)
  -> Sem r ([Upstream], a)
returnVesselCache m = do
  (deps, res) <- m
  output . Deposit CacheDependencies $ getUpstreamKey <$> deps
  return (deps, res)

observeVesselCache
  :: Members '[Output (Vessel Server), Output [Upstream]] r
  => Sem r a
  -> Sem r a
observeVesselCache = intercept @(Output (Vessel Server)) $ \case
  Output s -> do
    case find CacheDependencies s of
      Nothing -> return ()
      Just xs -> output $ Upstream <$> xs
    output s
