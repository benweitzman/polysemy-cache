{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}

module Cache.Dependency.Types where

import           Data.ByteString (ByteString)
import           Polysemy

newtype Upstream = Upstream { getUpstreamKey :: String } deriving (Show, Eq)
newtype Downstream = Downstream { getDownstreamKey :: String } deriving (Show, Eq)

data Cached a = Cached
  { cachedVal        :: a
  , cachedUpstream   :: [Upstream]
  , cachedDownstream :: [Downstream]
  } deriving (Show, Eq)


data DependencyCache m a where
  LookupCached :: String -> DependencyCache m (Maybe (Cached ByteString))
  SetVal :: String -> [Upstream] -> ByteString -> DependencyCache m ()
  SetDependency :: Downstream -> Upstream -> DependencyCache m ()
  DeleteKeys :: [String] -> DependencyCache m ()

makeSem ''DependencyCache
