{-# LANGUAGE ConstraintKinds     #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE Rank2Types          #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TupleSections       #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeOperators       #-}

module Cache.Dependency.Redis where

import           Cache.Dependency.Types
import           Cache.Types
import           Control.Monad
import           Data.Binary (Binary)
import qualified Data.Binary as B
import           Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BS8
import qualified Data.ByteString.Lazy as LBS
import           Data.Maybe
import           Database.Redis
import           Polysemy
import           Polysemy.Error
import           Polysemy.State

runRedisDependencyCache
  :: (Member (Embed Redis) r
     ,Member (Error Reply) r
     )
  => Sem (DependencyCache ': r) a -> Sem r a
runRedisDependencyCache = interpret $ \case
  DeleteKeys keys -> void $ fromEitherM @Reply @Redis $ del (BS8.pack <$> keys)

  LookupCached key -> do
    all <-fromEitherM @Reply @Redis $ hgetall (BS8.pack key)
    case lookup "~" all of
      Just cachedVal ->
        let cachedUpstream = flip mapMaybe all $ \(dKey,_) ->
              case BS8.unpack dKey of
                ('u':realKey) -> Just $ Upstream realKey
                _ -> Nothing
            cachedDownstream = flip mapMaybe all $ \(dKey,_) ->
              case BS8.unpack dKey of
                ('d':realKey) -> Just $ Downstream realKey
                _ -> Nothing
        in return $ Just Cached{..}
      Nothing -> return Nothing

  SetVal key upstream  _data ->
    void . fromEitherM @Reply @Redis . hmset (BS8.pack key) . (("~", _data):) $
      [(BS8.pack $ 'u':u, "")
      | Upstream u <- upstream
      ]

  SetDependency (Downstream downstream) (Upstream upstream) -> void $
    fromEitherM @Reply @Redis $ hset (BS8.pack upstream) (BS8.pack $ 'd':downstream) ""
