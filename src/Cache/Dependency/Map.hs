{-# LANGUAGE ConstraintKinds     #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE Rank2Types          #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TupleSections       #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeOperators       #-}

module Cache.Dependency.Map where

import           Cache.Dependency.Types
import           Cache.Types
import           Data.ByteString (ByteString)
import qualified Data.Set as S
import           Data.Map (Map)
import qualified Data.Map as M
import           Polysemy
import           Polysemy.State

type DependencyMap = Map String (Cached ByteString)

runMapDependencyCache
  :: forall r a .
     (Member (State DependencyMap) r)
  => Sem (DependencyCache ': r) a
  -> Sem r a
runMapDependencyCache = interpret $ \case
  DeleteKeys keys -> modify @DependencyMap . flip M.withoutKeys $ S.fromList keys

  LookupCached key -> do
    s <- get @DependencyMap
    return $ M.lookup key s

  SetVal key upstream _data -> do
    modify @DependencyMap $ M.insert key Cached
      { cachedVal = _data
      , cachedDownstream = []
      , cachedUpstream = upstream
      }

  SetDependency downstream (Upstream upstream) -> do
    modify @DependencyMap . flip M.adjust upstream $ \c -> c
      { cachedDownstream = downstream : cachedDownstream c
      }
